//
//  OCPCoreSDK.h
//  OCPCoreSDK
//
//  Created by Mark Pospesel on 3/6/19.
//  Copyright © 2019 Odyssey Computing, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for OCPCoreSDK.
FOUNDATION_EXPORT double OCPCoreSDKVersionNumber;

//! Project version string for OCPCoreSDK.
FOUNDATION_EXPORT const unsigned char OCPCoreSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OCPCoreSDK/PublicHeader.h>
#import <OCPCoreSDK/NSBundle+Language.h>

