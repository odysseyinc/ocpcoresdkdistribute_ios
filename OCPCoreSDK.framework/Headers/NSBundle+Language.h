//
//  NSBundle+Language.h
//  ios_language_manager
//
//  Created by Maxim Bilan on 1/10/15.
//  Copyright (c) 2015 Maxim Bilan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)

+ (NSString *)getLanguage; // returns custom language if any, otherwise System Language
+ (NSString *)getAssociatedLanguage; // nil means use System Language
+ (void)setLanguage:(NSString *)language; // set custom language (to override System Language)

@end

@interface LocaleEx : NSObject

@property (class, readonly, copy) NSLocale *current;    // an object representing the user's current locale (adjusted to custom language if necessary)

@end
