Pod::Spec.new do |s|
	s.name = 'OCPCoreSDKDistribute'
	s.version = '0.9.0'
	s.swift_version = '5.0'
	s.summary = 'Core Components for Odyssey Computing Platform'
	s.homepage = 'https://odysseyinc.com'
    	s.license = { :type => 'Custom', :file => 'LICENSE.md' }
	s.platform = :ios, '10.0'

	s.author = 'Mark Pospesel, Karim Alami, et al'

	s.source = { :git => 'https://bitbucket.org/odysseyinc/ocpcoresdkdistribute_ios.git', :tag => s.version }
	s.public_header_files = "OCPCoreSDK.framework/Headers/*.h"
    	s.source_files = "OCPCoreSDK.framework/Headers/*.h"
    	s.vendored_frameworks = "OCPCoreSDK.framework"
	s.frameworks = 'CoreData'
	s.dependency 'CocoaLumberjack/Swift'
	s.dependency 'DataCompression'
    	s.dependency 'KeychainAccess'
    	s.dependency 'libPhoneNumber-iOS'
	s.requires_arc = true
end
